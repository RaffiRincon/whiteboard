//
//  WhiteboardView.h
//  Whiteboard
//
//  Created by Rafael Rincon on 7/19/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WhiteboardView : UIView

@property(nonatomic,retain) UIResponder *viewControllerResponder;

@end
