//
//  main.m
//  Whiteboard
//
//  Created by Rafael Rincon on 7/19/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
