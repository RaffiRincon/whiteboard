//
//  WhiteboardImageView.m
//  Whiteboard
//
//  Created by Rafael Rincon on 7/22/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import "WhiteboardImageView.h"

@interface WhiteboardImageView()

@property UITouch *drawingTouch;
@property CGPoint lastPoint;
//@property UIImage *lastImage;
@property CGFloat strokeWidth;
@property BOOL scrolling;
@property BOOL zooming;

@end

@implementation WhiteboardImageView

-(void)awakeFromNib {
	_strokeWidth = 5.0;
	
	UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressedWithRecognizer:)];

	[self addGestureRecognizer:longPressRecognizer];
	
	UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(zoomedWithRecognizer:)];
	
	[self addGestureRecognizer:pinchRecognizer];
	
	self.image = [self imageWithColor:[UIColor whiteColor] andSize:CGSizeMake(self.bounds.size.width * 5, self.bounds.size.height * 5)];
}

#pragma mark - Touch-Related -

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if (touches.count == 1 && !_drawingTouch) {
		_drawingTouch = [[touches allObjects] firstObject];
		
//		NSLog(@"touch Cake!");
		_lastPoint = [_drawingTouch locationInView:self];
	}
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if (touches.count == 1 && _drawingTouch && [touches containsObject: _drawingTouch]) {
		if (_scrolling || _zooming) {
			return;
		}
		
		CGPoint currentPoint = [self.superview convertPoint:[_drawingTouch locationInView:self] toView:self];
		
		[self drawLineFromLastPointToPoint:currentPoint];
		_lastPoint = currentPoint;
	}
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if (touches.count == 1 && _drawingTouch && [touches containsObject:_drawingTouch]) {
		
		CGPoint currentPoint = [_drawingTouch locationInView:self];
		[self drawLineFromLastPointToPoint:currentPoint];
		
		_drawingTouch = nil;
	}
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if (touches.count == 1 && _drawingTouch && [touches containsObject:_drawingTouch]) {
		_drawingTouch = nil;
	}
//	if (touches)
//		[self touchesEnded:touches withEvent:event];
}

-(void)longPressedWithRecognizer:(UILongPressGestureRecognizer *)longPressRecognizer {
	if (longPressRecognizer.state == UIGestureRecognizerStateBegan) {
		_scrolling = true;
		NSLog(@"scrolling enabled!");
	} else if (longPressRecognizer.state == UIGestureRecognizerStateEnded) {
		_scrolling = false;
//		[_longPressDelegate longPressDidEnd];
		NSLog(@"scrolling disbaled!");
	}
	[_gestureDelegate didReceiveTouchDuringLongPressWithRecognizer:longPressRecognizer];
}

-(void)zoomedWithRecognizer:(UIPinchGestureRecognizer *)zoomRecognizer {
	if (zoomRecognizer.state == UIGestureRecognizerStateBegan) {
		_zooming = true;
	} else if (zoomRecognizer.state == UIGestureRecognizerStateEnded) {
		_zooming = false;
	}
	
	[_gestureDelegate didReceiveTouchDuringZoomWithRecognizer:zoomRecognizer];
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
	return  nil;
}

#pragma mark - Helper -

-(void)drawLineFromLastPointToPoint:(CGPoint)b {
//	if (_lastPoint.x < 0 || _lastPoint.x > self.bounds.size.width || _lastPoint.y < 0 || _lastPoint.y > self.bounds.size.height) {
//		return;
//	}
	
//	UIGraphicsPushContext(_context);
//	_context = UIGraphicsGetCurrentContext();
	[self.image drawInRect:self.frame];
	
	if (_drawingTouch == nil) {
		UIGraphicsEndImageContext();
		return;
	}
	CGContextMoveToPoint(_context, _lastPoint.x, _lastPoint.y);
	CGContextAddLineToPoint(_context, b.x, b.y);
	
	[self setUpContext:_context];
	
	CGContextStrokePath(_context);
	
	self.image = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
}

-(void)setUpContext:(CGContextRef)context {
	CGContextSetLineCap(context, kCGLineCapRound);
	CGContextSetLineWidth(context, _strokeWidth);
	CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);// TODO: make this variable
	CGContextSetBlendMode(context, kCGBlendModeNormal);
	CGContextSetAllowsAntialiasing(context, true);
	CGContextSetShouldAntialias(context, true);
}

-(UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size {
	CGRect rect = self.frame;
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, rect);
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return image;
}


@end
