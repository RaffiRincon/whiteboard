//
//  WhiteboardScrollView.m
//  Whiteboard
//
//  Created by Rafael Rincon on 7/22/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import "WhiteboardScrollView.h"
#import "WhiteboardImageView.h"

@interface WhiteboardScrollView()

@property (weak, nonatomic) IBOutlet WhiteboardImageView *imageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewTrailingConstraint;

@property CGPoint initialLocation;
@property CGPoint dragLocation;
@property CGRect contentFrameInternal;
@property CGFloat oldRecognizerScale;

@end

@implementation WhiteboardScrollView

#pragma - View Lifecycle -

-(void)awakeFromNib {
	self.scrollEnabled = false;
	_initialLocation = CGPointZero;
	_dragLocation = CGPointZero;
	_oldRecognizerScale = 0.0;
}

-(void)didMoveToSuperview {
	[super didMoveToSuperview];
	[self updateConstraintsForSize:self.bounds.size];
//	self.delaysContentTouches = false;
	self.contentSize = _imageView.frame.size;
}

-(void)layoutSubviews {
	[super layoutSubviews];
	_contentFrameInternal = self.bounds;
	
	self.minimumZoomScale = 1.0;
	self.maximumZoomScale = 5.0;
//	self.zoomScale = self.maximumZoomScale;
//	[self updateMinViewScaleForSize:self.bounds.size];
//	[self updateMaxZoomScaleForSize:self.bounds.size];
	
}

#pragma mark - Touch-Related -

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if (self.scrollEnabled) {
		return;
	}
	[_imageView touchesBegan:touches withEvent:event];
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if (self.scrollEnabled) {
		return;
	}
	
	[_imageView touchesMoved:touches withEvent:event];
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if (self.scrollEnabled) {
		return;
	}
	[_imageView touchesEnded:touches withEvent:event];
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	[self touchesEnded:touches withEvent:event];
}

-(void)longPressedWithRecognizer:(UILongPressGestureRecognizer *)longPressRecognizer {
	if (longPressRecognizer.state == UIGestureRecognizerStateBegan) {
		self.scrollEnabled = true;
		NSLog(@"scrolling enabled!");
	} else if (longPressRecognizer.state == UIGestureRecognizerStateEnded) {
		self.scrollEnabled = false;
		NSLog(@"scrolling disbaled!");
	}
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
	return true;
}

-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
	return  nil;
}

#pragma mark - Gesture Delegation -


-(void)didReceiveTouchDuringLongPressWithRecognizer:(UILongPressGestureRecognizer *)recognizer {
	if (recognizer.state == UIGestureRecognizerStateBegan) {
		_initialLocation = [recognizer locationInView:self];
	} else if (recognizer.state == UIGestureRecognizerStateChanged) {
		CGPoint currentLocation = [recognizer locationInView:self];
		CGPoint newLocation = CGPointMake(_initialLocation.x - currentLocation.x + _dragLocation.x, _initialLocation.y - currentLocation.y + _dragLocation.y);
		
		_dragLocation = newLocation;
		
		self.contentOffset = newLocation;
	} else if (recognizer.state == UIGestureRecognizerStateEnded) {
		CGPoint newOffset = self.contentOffset;
		BOOL shouldMove = false;
		if (self.contentOffset.x < 0) {
			newOffset.x = 0;
			shouldMove = true;
		}
		
		if (self.contentOffset.y < 0) {
			newOffset.y = 0;
			shouldMove = true;
		}
		
		if (self.contentOffset.x > _imageView.frame.size.width - self.frame.size.width) {
			newOffset.x = MAX(_imageView.frame.size.width - self.frame.size.width, 0);
			shouldMove = true;
		}
		
		if (self.contentOffset.y > _imageView.frame.size.height - self.frame.size.height) {
			newOffset.x = MAX(_imageView.frame.size.height - self.frame.size.height, 0);
			shouldMove = true;
		}
		
		if (shouldMove) {
			[UIView animateWithDuration:0.25 animations:^{
				self.contentOffset = newOffset;
			}];
		}
	}
}

-(void)didReceiveTouchDuringZoomWithRecognizer:(UIPinchGestureRecognizer *)recognizer {
	
	if (recognizer.state == UIGestureRecognizerStateBegan) {
		_oldRecognizerScale = 0.0;
	}
	
	if (_oldRecognizerScale == 0.0) {
		_oldRecognizerScale = recognizer.scale;
		return;
	}
	self.zoomScale += recognizer.scale - _oldRecognizerScale;
	_oldRecognizerScale = recognizer.scale;
}

//-(void)didReceiveTouchDuringLongPressWithLocation:(CGPoint)location {
//	CGPoint locationInSelf = [_imageView convertPoint:location toView:self];
//	
//	if (_lastScrollingPoint.x == -100.0 && _lastScrollingPoint.y == -100.0) {
//		_lastScrollingPoint = locationInSelf;
//		return;
//	}
//	
//	[self adjustContentFrameWithDX:locationInSelf.x - _lastScrollingPoint.x andDY:locationInSelf.y - _lastScrollingPoint.y];
//	_lastScrollingPoint = locationInSelf;
//}
//
-(void)longPressDidEnd {
//	_lastScrollingPoint = CGPointMake(-100.0, -100.0);
}

#pragma mark - UIScrollView Delegation -

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return _imageView;
}

#pragma mark - Helper -

//-(void)updateMinViewScaleForSize:(CGSize)size {
//	CGFloat widthScale = size.width / _imageView.bounds.size.width / 2;
//	CGFloat heightScale = size.height / _imageView.bounds.size.height / 2;
//	
//	CGFloat minScale = MIN(widthScale, heightScale);
//	NSLog(@"minScale: %f, maxScale: %f", self.minimumZoomScale, self.maximumZoomScale);
////	self.minimumZoomScale = minScale;
//	self.zoomScale = minScale;
//}
//
//-(void)updateMaxZoomScaleForSize:(CGSize)size {
//	CGFloat widthScale = size.width / _imageView.bounds.size.width;
//	CGFloat heightScale = size.height / _imageView.bounds.size.height;
//	
//	self.maximumZoomScale = MAX(widthScale, heightScale);
//}


-(void)updateConstraintsForSize:(CGSize)size {
	CGFloat yOffset = MAX(0, (size.height - _imageView.frame.size.height) / 2);
	_imageViewTopConstraint.constant = yOffset;
	_imageViewBottomConstraint.constant = yOffset;
 
	CGFloat xOffset = MAX(0, (size.width - _imageView.frame.size.width) / 2);
	_imageViewLeadingConstraint.constant = xOffset;
	_imageViewTrailingConstraint.constant = xOffset;
 
	[self layoutIfNeeded];
}

@end
