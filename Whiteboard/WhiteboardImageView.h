//
//  WhiteboardImageView.h
//  Whiteboard
//
//  Created by Rafael Rincon on 7/22/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WhiteboardImageView;
@protocol GestureDelegate <NSObject>

-(void)didReceiveTouchDuringLongPressWithRecognizer:(UILongPressGestureRecognizer *)recognizer;
-(void)didReceiveTouchDuringZoomWithRecognizer:(UIPinchGestureRecognizer *)recognizer;
-(void)longPressDidEnd;

@end

@interface WhiteboardImageView : UIImageView

@property (nonatomic, assign) id <GestureDelegate> gestureDelegate;

@property CGContextRef context;

@end
