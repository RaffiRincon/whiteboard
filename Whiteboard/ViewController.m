//
//  ViewController.m
//  Whiteboard
//
//  Created by Rafael Rincon on 7/19/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import "ViewController.h"
#import "WhiteboardView.h"
#import "WhiteboardScrollView.h"
#import "WhiteboardImageView.h"

@interface ViewController()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewTralingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewLeadingConstraint;

@property (weak, nonatomic) IBOutlet UIButton *redButton;
@property (weak, nonatomic) IBOutlet UIButton *blueButton;
@property (weak, nonatomic) IBOutlet UIButton *greenButton;
@property (weak, nonatomic) IBOutlet UIButton *yellowButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;

@property UITouch *drawingTouch;
@property UIColor *strokeColor;
@property CGPoint lastPoint;
@property CGPoint dragPoint;
@property CGPoint initialPoint;
@property CGFloat oldPinchRecognizerScale;
@property CGFloat pinchRecognizerScale;
@property CGFloat buttonWidth;
@property NSUInteger touchCount;

@end

@implementation ViewController

#pragma mark - ViewController Lifecycle -

- (void)viewDidLoad {
	[super viewDidLoad];
	
	// extends the board to be 5x width and height
	CGFloat xDiff = self.view.frame.size.width * 4;
	self.imageViewTralingConstraint.constant = -xDiff;
	
	CGFloat yDiff = self.view.frame.size.height * 4;
	self.imageViewBottomConstraint.constant = -yDiff;
	
	// set up buttons
	self.buttonWidth = 65.0;
	
	// set default values of scrollView
	self.scrollView.scrollEnabled = false;
	self.scrollView.delegate = self;
	self.scrollView.minimumZoomScale = 1.0;
	self.scrollView.maximumZoomScale = 5.0;
	
	// set other default values
	self.pinchRecognizerScale = 1.0;
	self.strokeColor =[UIColor blueColor];
	
	// add gesture recognizers
	UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(onZoomWithRecognizer:)];
	
	[self.view addGestureRecognizer:pinchRecognizer];
	
	UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPressWithRecognizer:)];
	
	[self.view addGestureRecognizer:longPressRecognizer];
}

-(void)viewDidLayoutSubviews {
//	[super viewDidLayoutSubviews];
	
	NSMutableArray<UIButton *> *buttons =[[NSMutableArray alloc] initWithObjects:
										  self.redButton,
										  self.blueButton,
										  self.greenButton,
										  self.yellowButton,
										  self.clearButton,
										  nil
										  ];
	NSArray<UIColor *> *colors = @[
								   [UIColor redColor],
								   [UIColor blueColor],
								   [UIColor greenColor],
								   [UIColor yellowColor],
								   [UIColor lightGrayColor]
								   ];
	
	CGFloat spacing = self.view.bounds.size.width / (CGFloat) buttons.count;
	
	for (int i = 0; i < buttons.count; i++) {
		CGPoint position = CGPointMake(+ spacing * ((CGFloat)i + 0.5), CGRectGetMaxY(self.view.bounds) - self.buttonWidth * 0.5 - spacing * 0.5);
		CGSize size = CGSizeMake(self.buttonWidth, self.buttonWidth);
		buttons[i].center = position;
		buttons[i].bounds = CGRectMake(0, 0, size.width, size.height);
		buttons[i].backgroundColor = colors[i];
	}
}

#pragma mark - Touch-Related -

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	self.touchCount += touches ? touches.count : 0;
	if (!self.drawingTouch) {
		self.drawingTouch = [[touches allObjects] firstObject];
		
//		self.lastPoint = [self.view convertPoint:[self.drawingTouch locationInView:self.view] toView:self.imageView];
		self.lastPoint = [self.drawingTouch locationInView:self.imageView];
	}
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	if (self.touchCount == 1 && self.drawingTouch && [touches containsObject:self.drawingTouch]) {
//		CGPoint currentPoint = [self.view convertPoint:[self.drawingTouch locationInView:self.view] toView:self.imageView];
		CGPoint currentPoint = [self.drawingTouch locationInView:self.imageView];
		[self drawLineFromLastPointToPoint:currentPoint];
		self.lastPoint = currentPoint;
	}
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	self.touchCount -= touches ? touches.count : 0;
	if (self.drawingTouch && [touches containsObject:self.drawingTouch]) {
		if (self.touchCount == 0 && touches.count == 1) {
			CGPoint currentPoint = [self.view convertPoint:[self.drawingTouch locationInView:self.view] toView:self.imageView];
			[self drawLineFromLastPointToPoint:currentPoint];
			self.lastPoint = currentPoint;
		}
		self.drawingTouch = nil;
	}
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
	self.touchCount -= touches ? touches.count : 0;
	if (self.drawingTouch) {
		self.drawingTouch = nil;
	}
}

#pragma mark - IBActions -

- (IBAction)onRedButtonTapped:(UIButton *)sender {
	self.strokeColor = [UIColor redColor];
}

- (IBAction)onBlueButtonTapped:(UIButton *)sender {
	self.strokeColor = [UIColor blueColor];
}

- (IBAction)onGreenButtonTapped:(UIButton *)sender {
	self.strokeColor = [UIColor greenColor];
}

- (IBAction)onYellowButtonTapped:(UIButton *)sender {
	self.strokeColor = [UIColor yellowColor];
}

- (IBAction)onClearButtonTapped:(UIButton *)sender {
	// truncate imageView's frame's properties to avoid weird bahavior
	self.imageView.bounds = CGRectMake(truncf(self.imageView.bounds.origin.x), truncf(self.imageView.bounds.origin.y), truncf(self.imageView.bounds.size.width), truncf(self.imageView.bounds.size.height));
	
	UIGraphicsBeginImageContext(self.imageView.bounds.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
	CGContextFillPath(context);
	self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
}

#pragma mark - Helper -

-(void)drawLineFromLastPointToPoint:(CGPoint)b {
	// truncate imageView's frame's properties to avoid weird bahavior
	self.imageView.bounds = CGRectMake(truncf(self.imageView.bounds.origin.x), truncf(self.imageView.bounds.origin.y), truncf(self.imageView.bounds.size.width), truncf(self.imageView.bounds.size.height));
	
	UIGraphicsBeginImageContext(self.imageView.bounds.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	[self.imageView.image drawInRect:self.imageView.bounds];
	
//	CGFloat xOffset = self.imageView.bounds.size.width * (self.pinchRecognizerScaleForStrokeWidth - 1.0);
//	CGFloat yOffset = self.imageView.bounds.size.height * (self.pinchRecognizerScaleForStrokeWidth - 1.0);
	
	NSLog(@"contentOffset: (%f, %f)", self.scrollView.contentOffset.x, self.scrollView.contentOffset.y);
	NSLog(@"self.lastPoint: (%f, %f)", self.lastPoint.x, self.lastPoint.y);
	NSLog(@"drawing point: (%f, %f)", self.lastPoint.x * self.pinchRecognizerScale, self.lastPoint.y * self.pinchRecognizerScale);
	NSLog(@"self.pinchRecognizerScale: %f", self.pinchRecognizerScale);
	NSLog(@"self.imageView.width: %f", self.imageView.frame.size.width);
	NSLog(@"self.imageView.height: %f\n\n", self.imageView.frame.size.height);
	
	CGContextMoveToPoint(context, self.lastPoint.x, self.lastPoint.y);
	CGContextAddLineToPoint(context, b.x, b.y);
	
	[self setUpContext:context];
	
	CGContextStrokePath(context);
	
	self.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
}

-(void)setUpContext:(CGContextRef)context {
	CGContextSetLineCap(context, kCGLineCapRound);
	CGContextSetLineWidth(context, 5.0);
	CGContextSetStrokeColorWithColor(context, self.strokeColor.CGColor);
	CGContextSetBlendMode(context, kCGBlendModeNormal);
	CGContextSetAllowsAntialiasing(context, true);
	CGContextSetShouldAntialias(context, true);
}

-(UIImage *)imageWithColor:(UIColor *)color width:(CGFloat)width andHeight:(CGFloat)height {
	CGSize size = CGSizeMake(width, height);
	UIGraphicsBeginImageContext(size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGContextSetFillColorWithColor(context, color.CGColor);
	CGContextAddRect(context, CGRectMake(0, 0, size.width, size.height));
	CGContextDrawPath(context, kCGPathFill);
	
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	
	UIGraphicsEndImageContext();
	
	return image;
}

-(void)updateConstraintsForSize:(CGSize)size {
	CGFloat yOffset = MAX(0, size.height);
	self.imageViewTopConstraint.constant = yOffset;
	self.imageViewBottomConstraint.constant = yOffset;
	
	CGFloat xOffset = MAX(0, size.width);
	self.imageViewLeadingConstraint.constant = xOffset;
	self.imageViewTralingConstraint.constant = xOffset;
}

#pragma mark - UIScrollView Delegation -

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	return self.imageView;
}

#pragma - Gesture Recognition -

-(void)onZoomWithRecognizer:(UIPinchGestureRecognizer *)pinchRecognizer {
	if (pinchRecognizer.state == UIGestureRecognizerStateBegan) {
		self.oldPinchRecognizerScale = -1.0;
	}
	
	if (self.oldPinchRecognizerScale == -1.0) {
		self.oldPinchRecognizerScale = pinchRecognizer.scale;
		return;
	}
	
	self.scrollView.zoomScale += pinchRecognizer.scale - self.oldPinchRecognizerScale;
	NSLog(@"\n\nscrollView zoomScale: %f\npinchRecognizer scale: %f\n", self.scrollView.zoomScale, pinchRecognizer.scale);
	self.pinchRecognizerScale = self.scrollView.zoomScale;
	
	self.oldPinchRecognizerScale = pinchRecognizer.scale;
//	[self updateConstraintsForSize:self.view.frame.size];
}

-(void)onLongPressWithRecognizer:(UILongPressGestureRecognizer *)recognizer {
	if (recognizer.state == UIGestureRecognizerStateBegan) {
		self.initialPoint = [recognizer locationInView:self.imageView];
	} else if (recognizer.state == UIGestureRecognizerStateChanged) {
		CGPoint currentLocation = [recognizer locationInView:self.imageView];
		CGPoint newPoint = CGPointMake(self.initialPoint.x - currentLocation.x + self.dragPoint.x, self.initialPoint.y - currentLocation.y + self.dragPoint.y);
		
		self.dragPoint = newPoint;
		
		self.scrollView.contentOffset = newPoint;
	} else if (recognizer.state == UIGestureRecognizerStateEnded) {
		CGPoint newOffset = self.scrollView.contentOffset;
		BOOL shouldMove = false;
		if (self.scrollView.contentOffset.x < 0) {
			newOffset.x = 0;
			shouldMove = true;
		}
		
		if (self.scrollView.contentOffset.y < 0) {
			newOffset.y = 0;
			shouldMove = true;
		}
		
		if (self.scrollView.contentOffset.x > self.imageView.frame.size.width - self.scrollView.frame.size.width) {
			newOffset.x = MAX(self.imageView.frame.size.width - self.scrollView.frame.size.width, 0);
			shouldMove = true;
		}
		
		if (self.scrollView.contentOffset.y > self.imageView.frame.size.height - self.scrollView.frame.size.height) {
			newOffset.x = MAX(self.imageView.frame.size.height - self.scrollView.frame.size.height, 0);
			shouldMove = true;
		}
		
		if (shouldMove) {
			[UIView animateWithDuration:0.25 animations:^{
				self.scrollView.contentOffset = newOffset;
			}];
		}
	}
}

@end












