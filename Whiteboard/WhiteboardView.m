//
//  WhiteboardView.m
//  Whiteboard
//
//  Created by Rafael Rincon on 7/19/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import "WhiteboardView.h"
#import "WhiteboardImageView.h"
#import "WhiteboardScrollView.h"

@interface WhiteboardView()

//@property UITouch *drawingTouch;
//@property CGPoint lastPoint;
//@property UIImage *lastImage;
//@property UIColor *strokeColor;
//@property CGFloat strokeWidth;
//@property UILongPressGestureRecognizer *longPressRecognizer;
//@property CGFloat scaleOffset;
//@property CGFloat maxWidth;
//@property CGFloat xOffset;
//@property CGFloat yOffset;
//@property BOOL userIsLongPressing;
//@property BOOL imageMirrored;


@property (weak, nonatomic) IBOutlet WhiteboardScrollView *scrollView;

@end

@implementation WhiteboardView

@synthesize viewControllerResponder;

#pragma mark - Touch-Related -

//-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//	return  nil;
//}

//-(void)awakeFromNib {
//	_pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(onViewPinched:)];
//	[_imageView addGestureRecognizer:_pinchRecognizer];
//	
//	_longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
//	[_imageView addGestureRecognizer:_longPressRecognizer];
//	
//	// experiment to get higher resolution lines
//	_paths = [NSMutableArray<UIBezierPath *> new];
//	
//	_strokeColor = [UIColor blueColor];
//	_strokeWidth = 10.0;
//	_scaleOffset = 0.0;
//	_maxWidth = self.frame.size.width * 10;
//	_xOffset = 0.0;
//	_yOffset = 0.0;
//	_lastImage = [self imageWithColor:[UIColor whiteColor]];
//	_userIsLongPressing = false;
//	_imageMirrored = false;
//}
//
//-(void)layoutSubviews {
//	[super layoutSubviews];
//	
//	// set canvas to be 5x width and 5x height
//	_imageView.frame = CGRectMake(0, 0, self.frame.size.width * 10, self.frame.size.height * 10);
//}
//
//#pragma mark - Touch-Related -
//
//-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//	if (touches.count == 1 && !_drawingTouch) {
//		_drawingTouch = [[touches allObjects] firstObject];
//		
////		// debugging
////		CGPoint anchor = [self convertPoint:self.center toView:_imageView];
////		NSLog(@"%s", anchor.x == self.center.x ? "Cake" : "No Cake");
//		
//		_lastPoint = [_drawingTouch locationInView:self];
//		[_paths addObject:[UIBezierPath new]];
//		if (_paths.count > 1)
//			[[_paths lastObject] appendPath:_paths[_paths.count - 2]];
//		[[_paths lastObject] moveToPoint:[self convertPoint:_lastPoint toView:_imageView]];
//	}
//	NSLog(@"self width:\t%f\nimageView width:\t%f", self.frame.size.width, _imageView.frame.size.width);
//}
//
//-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//	if (touches.count == 1 && _drawingTouch && [touches containsObject: _drawingTouch]) {
//		CGPoint currentPoint = [_drawingTouch locationInView:self];
//		[self drawLineFromLastPointToPoint:currentPoint];
////		[self drawPathFromLastPointToPoint:currentPoint];
//		_lastPoint = currentPoint;
//	}
//}
//
//-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//	if (touches.count == 1 && _drawingTouch && [touches containsObject:_drawingTouch]) {
//		
//		CGPoint currentPoint = [_drawingTouch locationInView:self];
//		[self drawLineFromLastPointToPoint:currentPoint];
////		[self drawPathFromLastPointToPoint:currentPoint];
//		
////		UIGraphicsBeginImageContext(_imageView.frame.size);
////		CGContextRef context = UIGraphicsGetCurrentContext();
////		
////		for (UIBezierPath *path in _paths) {
////			CGContextAddPath(context, path.CGPath);
////		}
////		
////		[self setUpContext:context];
////		
////		CGContextStrokePath(context);
////		
////		_imageView.image = UIGraphicsGetImageFromCurrentImageContext();
////		
////		
////		UIGraphicsEndImageContext();
//		
//		_lastImage = _imageView.image;
//		
//		_drawingTouch = nil;
//		_userIsLongPressing = false;
//	}
//}
//
//-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//	if (touches)
//		[self touchesEnded:touches withEvent:event];
//}
//
//#pragma mark - Helper -
//						
//-(void)onViewPinched:(UIPinchGestureRecognizer *)sender {
////	NSLog(@"View width:\t%f", self.frame.size.width);
//	
//	CGSize newSize = CGSizeMake(_imageView.frame.size.width * sender.scale, _imageView.frame.size.height * sender.scale);
//	if (newSize.width < _maxWidth && newSize.width > self.frame.size.width) {
////		CGPoint anchor = [self convertPoint:self.center toView:_imageView];
//		
//		CGPoint centerScreen = [self convertPoint:self.center toView:_imageView];
//		CGRect oldFrame = _imageView.frame;
//		[_imageView.layer setAnchorPoint: CGPointMake((centerScreen.x + _xOffset) / _imageView.frame.size.width, (centerScreen.y + _yOffset) / _imageView.frame.size.height)];
//		_imageView.frame = oldFrame;
//		
//		_imageView.transform = CGAffineTransformScale(_imageView.transform, sender.scale, sender.scale);
//		
////		CGPoint newAnchor = [self convertPoint:self.center toView:_imageView];
////		CGPoint newCenter = _imageView.center;
////		newCenter.x -= anchor.x - newAnchor.x;
////		newCenter.y -= anchor.y - newAnchor.y;
////		
////		_imageView.center = newCenter;
//	}
//	
////	_imageView.image = _lastImage;
//}
//
//-(void)resetToLastImageAndZoomWithScale:(CGFloat) scale {
//	if(!_lastImage)
//		return;
//	_lastImage = [[UIImage alloc] initWithCGImage:_lastImage.CGImage scale:scale orientation:_lastImage.imageOrientation];
//	
//	
//	_imageView.image = _lastImage;
//}
//
//-(void)drawLineFromLastPointToPoint:(CGPoint)b {
//	UIGraphicsBeginImageContext(_imageView.frame.size);// setting to _imageView instead of self causes memory leak problems and bugs
//	CGContextRef context = UIGraphicsGetCurrentContext();
//	
//	[_imageView.image drawInRect:_imageView.frame];//self.frame.size.width, self.frame.size.height)];
////	CGContextDrawImage(context, _imageView.frame, _imageView.image.CGImage);
//	
//	CGPoint correctedA = [self convertPoint:_lastPoint toView:_imageView];
//	CGPoint correctedB = [self convertPoint:b toView:_imageView];
//	
//	CGContextMoveToPoint(context, correctedA.x, correctedA.y);// maybe divide by ten
//	CGContextAddLineToPoint(context, correctedB.x, correctedB.y);// maybe divide by ten
//	
//	[self setUpContext:context];
//	
//	CGContextStrokePath(context);
//	
//	_imageView.image = UIGraphicsGetImageFromCurrentImageContext();
//	_testImageView.image = _imageView.image;
//	
//	UIGraphicsEndImageContext();
//}
//
//-(void)drawPathFromLastPointToPoint:(CGPoint)b {
//	UIGraphicsBeginImageContext(_imageView.frame.size);
//	CGContextRef context = UIGraphicsGetCurrentContext();
//	
//	CGContextDrawImage(context, _imageView.frame, _imageView.image.CGImage);
//	
//	[self setUpContext:context];
//	
//	CGPoint adjustedB = [self convertPoint:b toView:_imageView];
//	[[_paths lastObject] addLineToPoint:adjustedB];
//	
//	CGContextAddPath(context, [_paths lastObject].CGPath);
//	
//	CGContextStrokePath(context);
//	
////	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
////	if(_imageMirrored) {
////		image = [self flipImageVertically:image];
////	}
//	
//	_imageView.image = UIGraphicsGetImageFromCurrentImageContext();
//	_testImageView.image = _imageView.image;
//	
//	UIGraphicsEndImageContext();
//	
//	_imageMirrored = !_imageMirrored;
//}
//
//-(UIImage *) flipImageVertically:(UIImage *)originalImage {
//	UIImageView *tempImageView = [[UIImageView alloc] initWithImage:originalImage];
//	
//	UIGraphicsBeginImageContext(tempImageView.frame.size);
//	CGContextRef context = UIGraphicsGetCurrentContext();
//	CGAffineTransform flipVertical = CGAffineTransformMake(
//														   1, 0, 0, -1, 0, tempImageView.frame.size.height
//														   );
//	CGContextConcatCTM(context, flipVertical);
//	
//	[tempImageView.layer renderInContext:context];
//	
//	UIImage *flipedImage = UIGraphicsGetImageFromCurrentImageContext();
//	UIGraphicsEndImageContext();
//	
//	return flipedImage;
//}
//
//-(UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
//	UIGraphicsBeginImageContext(size);
//	[image drawInRect:CGRectMake(0, 0, size.width, size.height)];
//	UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
//	UIGraphicsEndImageContext();
//	return destImage;
//}
//
//-(void)onLongPress:(UILongPressGestureRecognizer *)sender {
//	NSLog(@"user longpressed!");
//	if (sender.state == UIGestureRecognizerStateBegan) {
//		_userIsLongPressing = true;
//	} else if (sender.state == UIGestureRecognizerStateEnded) {
//		_userIsLongPressing = false;
//	}
//}
//
//-(UIImage *)imageWithColor:(UIColor *)color {
//	CGRect rect = self.frame;
//	UIGraphicsBeginImageContext(rect.size);
//	CGContextRef context = UIGraphicsGetCurrentContext();
//	
//	CGContextSetFillColorWithColor(context, [color CGColor]);
//	CGContextFillRect(context, rect);
//	
//	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//	UIGraphicsEndImageContext();
//	
//	return image;
//}
//
//-(void)setUpContext:(CGContextRef)context {
//	CGContextSetLineCap(context, kCGLineCapRound);
//	CGContextSetLineWidth(context, _strokeWidth);
//	CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);// TODO: make this variable
//	CGContextSetBlendMode(context, kCGBlendModeNormal);
//	CGContextSetAllowsAntialiasing(context, true);
//	CGContextSetShouldAntialias(context, true);
//}

@end
