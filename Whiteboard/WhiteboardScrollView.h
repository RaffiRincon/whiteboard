//
//  WhiteboardScrollView.h
//  Whiteboard
//
//  Created by Rafael Rincon on 7/22/16.
//  Copyright © 2016 CornerGames. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WhiteboardScrollView : UIScrollView

@property(nonatomic,retain) UIResponder *viewControllerResponder;

@end
